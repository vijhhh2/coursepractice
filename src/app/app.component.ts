import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 serverElements = [{type: 'server', name: 'vijay', content: 'vijay'}];

 onServeradd(serverdata: {serverName: string, serverContent: string}) {
       this.serverElements.push({
      type: 'server',
      name: serverdata.serverName,
      content: serverdata.serverContent
    });
  }

  onBlueprintadd(blueprintdata: {serverName: string, serverContent: string}) {
       this.serverElements.push({
      type: 'blueprint',
      name: blueprintdata.serverName,
      content: blueprintdata.serverContent
    });
  }
  onChanges() {
    this.serverElements[0].name = 'changed';
  }
  onDestroy() {
    this.serverElements.splice(0, 1);
  }
}
