import { Component, OnInit, Input, ViewChild, ElementRef, ContentChild  } from '@angular/core';
import { OnChanges, SimpleChanges, DoCheck,
  AfterContentInit, AfterContentChecked,
   AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
@Component({
  selector: 'app-serverelements',
  templateUrl: './serverelements.component.html',
  styleUrls: ['./serverelements.component.css']
})
export class ServerelementsComponent implements OnInit,
OnChanges, DoCheck,
 AfterContentInit,
  AfterContentChecked,
AfterViewInit, AfterViewChecked,
OnDestroy {
  @Input() element: {name: string, type: string, content: string};
  @Input() name;
  @ViewChild('heading') header: ElementRef;
  @ContentChild('content') Content: ElementRef;
  constructor() {
    console.log('construcor called');
   }
   ngOnChanges(changes: SimpleChanges) {
    console.log('OnChanges called');
    console.log(changes);
   }

  ngOnInit() {
    console.log('oninit called');
    console.log('Textcontent' + this.header.nativeElement.textContent);
    console.log('ParagraphContent' + this.Content.nativeElement.textContent);
  }
  ngDoCheck() {
    console.log('docheck called');
  }
  ngAfterContentInit() {
    console.log('Aftercheckinit called');
    console.log('ParagraphContent' + this.Content.nativeElement.textContent);

  }
  ngAfterContentChecked() {
    console.log('Aftercontentcheck called');
  }
  ngAfterViewInit() {
    console.log('ngAfterViewInit called');
    console.log('Textcontent' + this.header.nativeElement.textContent);
  }
  ngAfterViewChecked() {
    console.log('ngAfterViewChecked called');
  }
  ngOnDestroy() {
    console.log('ngOnDestroy called');
  }

}
